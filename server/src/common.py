import binascii
import hashlib
import sqlite3
import sys
import uuid
import flask

# NOTE: This relies on the code being run from the
# root of the server directory. Adjust as necessary.
DATABASE_PATH = "data/data.db"

# Returns the SQLite database connection used
# by this Flask app.
def get_db():
    db = getattr(flask.g, "db", None)
    if not db:
        db = flask.g.db = sqlite3.connect(DATABASE_PATH)
        db.execute("PRAGMA foreign_keys = ON")
    return db

# Called when the application is shutting down.
def teardown_app(ex):
    db = getattr(flask.g, "db", None)
    if db:
        db.close()

# Returns a random version 4 UUID as a hexadecimal string.
def generate_uuid():
    return uuid.uuid4().hex

# Returns a Flask response object for a successful request
# with the specified HTTP code and optionally some data to
# include with the response JSON.
def response_ok(code, data=None):
    json = {"success": True}
    if data:
        json.update(data)
    resp = flask.jsonify(json)
    resp.status_code = code
    return resp

# Returns a Flask response object for a failed request
# with the specified HTTP code and an error message.
def response_error(code, msg):
    json = {"success": False, "message": msg}
    resp = flask.jsonify(json)
    resp.status_code = code
    return resp

# Hashes the given password using the specified salt.
# Returns the hash as a 32-byte bytearray object.
def hash_password(pwd, salt):
    return binascii.hexlify(
        hashlib.pbkdf2_hmac(
            "sha256",
            pwd.encode("utf-8"),
            binascii.unhexlify(salt),
            100000))

# Helper function for parsing the body of a request as JSON.
# Keys is a dict that maps keys to their expected type. For
# example, {"username": str}. Returns a tuple of (JSON dict,
# error str). JSON will be None if there are any errors in
# the JSON, or there are any missing/extra keys in the JSON.
def get_json(req, keys):
    result = {}
    json = req.get_json(force=True, silent=True)
    if json is None:
        return (None, "Request body is not valid JSON")
    for k, t in keys.items():
        v = json.pop(k, None)
        if not isinstance(v, t):
            return (None, f"Expected key '{k}' to have type '{t}'")
        result[k] = v
    if json:
        return (None, f"Unrecognized keys: {', '.join(json.keys())}")
    return (result, None)


# Authenticates the user given the username and password
# from a flask request. Returns a tuple of (user ID, message),
# where user ID is either the user ID as a string if login
# was successful, or None otherwise, and message is the
# reason for a login failure.
def authenticate(auth):
    if not auth:
        return (None, "Authentication required")

    # Unpack tuple (used in testing)
    if isinstance(auth, tuple):
        email, password = auth
    else:
        email, password = auth.username, auth.password

    # Load matching user from database
    db = get_db()
    result = db.execute("""
        SELECT id, password_salt, password_hash FROM User
        WHERE email = ?
        LIMIT 1
    """, (email,)).fetchone()

    # First check to see if any rows matched the username
    if not result:
        return (None, "User not found")
    user_id, pw_salt, pw_hash = result

    # Next hash provided password and compare it to the stored hash
    # For compatibility, allow login with either password or token
    if pw_hash == hash_password(password, pw_salt):
        return (user_id, None)

    # Try a token instead
    result = db.execute("""
        SELECT token FROM LoginToken
        WHERE user_id = ?
        LIMIT 1
    """, (user_id,)).fetchone()
    if not result:
        return (None, "Incorrect password or token")
    return (user_id, None)
