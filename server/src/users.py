import re
import sqlite3
import flask
import common

users = flask.Blueprint("users", __name__)

# Handles user creation (POST to /users).
@users.route("/users", methods=["POST"])
def create_user():
    json, err = common.get_json(flask.request, {
        "email": str,
        "password": str,
        "name": str
    })
    if not json:
        return common.response_error(400, err)
    email = json["email"]
    password = json["password"]
    name = json["name"]

    # Maybe have some complexity rules -- for now just check for empty
    if len(password) == 0:
        return common.response_error(400, "Empty password")

    # Interesting note: we could technically just use the
    # user ID as the salt, since both are randomly generated
    # UUID values.
    user_id = common.generate_uuid()
    password_salt = common.generate_uuid()
    password_hash = common.hash_password(password, password_salt)

    # Try to insert user into database. Since email column
    # is unique, insert will fail if user already exists.
    db = common.get_db()
    try:
        db.execute("""
            INSERT INTO User(id, email, password_salt, password_hash, name)
            VALUES(?, ?, ?, ?, ?)
        """, (user_id, email, password_salt, password_hash, name))
        db.commit()
    except sqlite3.IntegrityError:
        return common.response_error(409, "User already exists")

    return common.response_ok(201, {
        "user_id": user_id
    })

# Returns information about a user.
@users.route("/users/<user_id>", methods=["GET"])
def get_user(user_id):
    db = common.get_db()

    # Get profile info
    profile = db.execute("""
        SELECT email, name, phone, website, biography
        FROM User
        WHERE id = ?
        LIMIT 1
    """, (user_id,)).fetchone()
    if not profile:
        return common.response_error(404, "User not found")

    # Also retrieve skills
    skills = []
    query = db.execute("""
        SELECT name
        FROM Tag
        WHERE owner_id = ?
        AND type = 'skill'
    """, (user_id,))
    for row in query:
        skills.append(row[0])

    return common.response_ok(200, {
        "email": profile[0],
        "name": profile[1],
        "phone": profile[2],
        "website": profile[3],
        "biography": profile[4],
        "skills": skills
    })

# Updates a user's information. This can also be used
# to change your password.
@users.route("/users/<user_id>", methods=["PUT"])
def put_user(user_id):
    auth_user_id, err = common.authenticate(flask.request.authorization)
    if not auth_user_id:
        return common.response_error(403, err)
    if user_id != auth_user_id:
        return common.response_error(403, "Cannot change other user's info")

    # Parse JSON args
    json, err = common.get_json(flask.request, {
        "password": (str, type(None)),
        "email": (str, type(None)),
        "name": (str, type(None)),
        "phone": (str, type(None)),
        "website": (str, type(None)),
        "biography": (str, type(None)),
        "skills": (list, type(None)),
    })
    if err:
        return common.response_error(400, err)

    # Special case for password - generate new salt and hash
    new_password = json.pop("password", None)
    if new_password is not None:
        if len(new_password) == 0:
            return common.response_error(400, "Empty password")
        json["password_salt"] = common.generate_uuid()
        json["password_hash"] = common.hash_password(new_password, password_salt)

    # First pop skills...
    new_skills = json.pop("skills", None)

    # JSON to SQL parameterized query
    query_fields = []
    arg_fields = []
    for k, v in json.items():
        if v is not None:
            query_fields.append(f"{k} = ?")
            arg_fields.append(v)
    arg_fields.append(auth_user_id)

    db = common.get_db()
    if len(query_fields) != 0:
        try:
            # Update user fields
            db.execute(f"""
                UPDATE User
                SET {', '.join(query_fields)}
                WHERE id = ?
            """, arg_fields)
        except sqlite3.IntegrityError:
            return common.response_error(409, "Email already in use")

    # Update skills
    if new_skills is not None:
        # Delete original tags
        db.execute("""
            DELETE FROM Tag
            WHERE owner_id = ?
            AND type = 'skill'
        """, (auth_user_id,))

        # Insert new tags
        db.executemany("""
            INSERT INTO Tag(owner_id, type, name)
            VALUES(?, 'skill', ?)
        """, (map(lambda s: (auth_user_id, s), new_skills)))

    # Commit transaction
    db.commit()

    return common.response_ok(200)

# Handles login requests (POST to /login). Returns a
# token that can be used to authenticate future requests.
# Just replace your password with the token in the auth
# headers.
@users.route("/login", methods=["POST"])
def login():
    user_id, err = common.authenticate(flask.request.authorization)
    if not user_id:
        return common.response_error(403, err)

    # Generate and persist token
    token = common.generate_uuid()
    db = common.get_db()
    db.execute("""
        INSERT INTO LoginToken(user_id, token)
        VALUES(?, ?)
    """, (user_id, token))
    db.commit()

    return common.response_ok(200, {
        "user_id": user_id,
        "token": token,
    })

# Handles logout requests (POST to /logout). Deletes
# the token used to authenticate. If authentication
# is done with a password, this does nothing.
@users.route("/logout", methods=["POST"])
def logout():
    user_id, err = common.authenticate(flask.request.authorization)
    if not user_id:
        return common.response_error(403, err)

    # We're going to invalidate the token used to authenticate.
    # If it's actually a password, no harm done. This will just
    # be a no-op.
    token = flask.request.authorization.password

    # Delete token
    db = common.get_db()
    db.execute("""
        DELETE FROM LoginToken
        WHERE user_id = ? AND token = ?
    """, (user_id, token))
    db.commit()

    return common.response_ok(200)

# Returns a list of user's experience (inclues projects)
@users.route("/users/<user_id>/experience", methods=["GET"])
def get_experience(user_id):
    db = common.get_db()
    cursor = db.execute("""
        SELECT id, title, description, organization, location
        FROM UserExperience
        WHERE user_id = ?
    """, (user_id,))

    results = []
    for row in cursor:
        skills = list(map(lambda r: r[0], db.execute("""
            SELECT name
            FROM Tag
            WHERE owner_id = ?
            AND type = 'skill'
        """, (row[0],))))

        results.append({
            "id": row[0],
            "title": row[1],
            "description": row[2],
            "organization": row[3],
            "location": row[4],
            "skills": skills
        })

    return common.response_ok(200, {
        "results": results
    })

# Returns a single experience given the ID
@users.route("/users/experience/<exp_id>", methods=["GET"])
def get_experience_by_experience_id(exp_id):
    db = common.get_db()
    cursor = db.execute("""
        SELECT id, title, description, organization, location
        FROM UserExperience
        WHERE id = ?
    """, (exp_id,)).fetchone()

    results = {
        "id": cursor[0],
        "title": cursor[1],
        "description": cursor[2],
        "organization": cursor[3],
        "location": cursor[4]
    }

    skills = list(map(lambda r: r[0], db.execute("""
        SELECT name
        FROM Tag
        WHERE owner_id = ?
        AND type = 'skill'
    """, (results["id"],))))
    results["skills"] = skills

    return common.response_ok(200, {
        "data": results
    })

# Adds a new experience item for the given user
@users.route("/users/experience", methods=["POST"])
def add_experience():
    auth_user_id, err = common.authenticate(flask.request.authorization)
    if not auth_user_id:
        return common.response_error(403, err)
    # if user_id != auth_user_id:
    #     return common.response_error(403, "Cannot change other user's info")

    json, err = common.get_json(flask.request, {
        "title": str,
        "description": (str, type(None)),
        "organization": (str, type(None)),
        "location": (str, type(None)),
        "skills": (list, type(None))
    })
    if err:
        return common.response_error(400, err)

    title = json["title"]
    description = json["description"]
    organization = json["organization"]
    location = json["location"]
    skills = json["skills"]

    if skills and not all(map(lambda x: isinstance(x, str), skills)):
        return common.response_error(400, "Invalid skills")

    db = common.get_db()
    exp_id = common.generate_uuid()
    db.execute("""
        INSERT INTO UserExperience
        VALUES(?, ?, ?, ?, ?, ?)
    """, (exp_id, auth_user_id, title, description, organization, location))
    if skills is not None:
        db.executemany("""
            INSERT INTO Tag
            VALUES(?, 'skill', ?)
        """, (map(lambda s: (exp_id, s), skills)))
    db.commit()

    return common.response_ok(200, {
        "exp_id": exp_id
    })

# Modifies an existing experience entry.
@users.route("/users/experience/<exp_id>", methods=["PUT"])
def update_experience(exp_id):
    auth_user_id, err = common.authenticate(flask.request.authorization)
    if not auth_user_id:
        return common.response_error(403, err)
    # if user_id != auth_user_id:
    #     return common.response_error(403, "Cannot change other user's info")

    json, err = common.get_json(flask.request, {
        "title": (str, type(None)),
        "description": (str, type(None)),
        "organization": (str, type(None)),
        "location": (str, type(None)),
        "skills": (list, type(None))
    })
    if err:
        return common.response_error(400, err)

    skills = json.pop("skills", None)
    if skills and not all(map(lambda x: isinstance(x, str), skills)):
        return common.response_error(400, "Invalid skills")

    query_fields = []
    arg_fields = []
    for k, v in json.items():
        if v is not None:
            query_fields.append(f"{k} = ?")
            arg_fields.append(v)

    db = common.get_db()
    if len(query_fields) != 0:
        if db.execute(f"""
            UPDATE UserExperience
            SET {', '.join(query_fields)}
            WHERE id = ?
            AND user_id = ?
        """, arg_fields + [exp_id, auth_user_id]).rowcount == 0:
            return common.response_error(404, "Experience does not exist")

    if skills is not None:
        db.execute("""
            DELETE FROM Tag
            WHERE owner_id = ?
        """, (exp_id,))
        db.executemany("""
            INSERT INTO Tag
            VALUES(?, 'skill', ?)
        """, (map(lambda s: (exp_id, s), skills)))

    db.commit()
    return common.response_ok(200)

# Deletes an existing experience entry.
@users.route("/users/experience/<exp_id>", methods=["DELETE"])
def remove_experience(exp_id):
    auth_user_id, err = common.authenticate(flask.request.authorization)
    if not auth_user_id:
        return common.response_error(403, err)
    # if user_id != auth_user_id:
    #     return common.response_error(403, "Cannot change other user's info")

    db = common.get_db()
    if db.execute("""
        DELETE FROM UserExperience
        WHERE id = ?
        AND user_id  = ?
    """, (exp_id, auth_user_id)).rowcount == 0:
        return common.response_error(404, "Experience does not exist")

    db.execute("""
        DELETE FROM Tag
        WHERE owner_id = ?
    """, (exp_id,))

    db.commit()
    return common.response_ok(200)

# Adds a new education item for the given user
@users.route("/users/education", methods=["POST"])
def add_education():
    auth_user_id, err = common.authenticate(flask.request.authorization)
    if not auth_user_id:
        return common.response_error(403, err)
    # if user_id != auth_user_id:
    #     return common.response_error(403, "Cannot change other user's info")
    json, err = common.get_json(flask.request, {
        "school_name": str,
        "level": (str, type(None)),
        "gpa": (float, int, type(None)),
        "major": (str, type(None))
    })
    if err:
        return common.response_error(400, err)

    school_name = json["school_name"]
    level = json["level"]
    gpa = json["gpa"]
    major = json["major"]   

    db = common.get_db()
    edu_id = common.generate_uuid()
    db.execute("""
        INSERT INTO UserEducation
        VALUES(?, ?, ?, ?, ?, ?)
    """, (edu_id, auth_user_id, school_name, level, gpa, major))
    db.commit()

    return common.response_ok(200, {
        "edu_id": edu_id
    })

# Get a user's education by ID
def get_education(user_id):
    db = common.get_db()
    query = """
        SELECT school_name, level, gpa, major
        FROM UserEducation
        WHERE user_id = ?
    """

    cursor = db.execute(query, (user_id,))
    results = []
    for row in cursor:
        results.append({
            "school_name": row[0],
            "level": row[1],
            "gpa": row[2],
            "major": row[3]
        })

    return results