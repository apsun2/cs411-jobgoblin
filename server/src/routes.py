import flask
import applications
import base64
import common
import users
import jobs
import json

routes = flask.Blueprint("routes", __name__)

@routes.route("/")
def index():
    return flask.render_template("index.html",
        loggedin=len(flask.request.cookies.get('authToken', '')) > 0)

@routes.route("/create")
def create():
    return flask.render_template("create.html")

@routes.route("/signin")
def signin():
    return flask.render_template("login.html")

@routes.route("/profile")
def profile():
    authToken = str.encode(flask.request.cookies.get("authToken", ""), "utf-8")
    if authToken is None or len(authToken)==0:
        return common.response_error(500, "You are not logged in (please clear cookies if problem persists)")
    authToken = base64.b64decode(authToken)
    authToken = tuple(bytes.decode(authToken).split(":"))
    user_id, err = common.authenticate(authToken)
    if err:
        return common.response_error(500, "You are not logged in (please clear cookies if problem persists)")
    user_info = users.get_user(user_id)
    user_data = json.loads(user_info.data)
    user_data["userID"] = user_id
    postedJobs = jobs.get_user_jobs(user_id)
    return flask.render_template("profile.html", data=user_data, jobs=postedJobs)

@routes.route("/jobs/create")
def createJob():
    return flask.render_template("createjob.html")

@routes.route("/jobs/search")
def searchJobs():
    return flask.render_template("searchjobs.html")

@routes.route("/jobs/handleQuery", methods=["POST"])
def queryJobs():
    json, err = common.get_json(flask.request, {
        "query": list
    })
    if not json:
        return common.response_error(400, err)
    joblist = jobs.query_jobs_helper(json)
    return common.response_ok(200, {
        "template": flask.render_template("jobsearchresults.html", jobList=joblist)
    })

@routes.route("/applications/<job_id>", methods=["GET"])
def viewApplications(job_id):
    return flask.render_template("viewapplications.html", jobId=job_id)

@routes.route("/jobs/<job_id>/viewApplications", methods=["GET"])
def getApplicationsForJob(job_id):
    user_id, err = common.authenticate(flask.request.authorization)
    if not user_id:
        return common.response_error(403, err)
    apps = applications.get_applications_helper(job_id, user_id)
    return common.response_ok(200, {
        "template": flask.render_template("applications.html", applications=apps, jobId=job_id)
    })

@routes.route("/experience/currentUserExperience", methods=["GET"])
def getExperiences():
    user_id, err = common.authenticate(flask.request.authorization)
    if not user_id:
        return common.response_error(403, err)
    experienceList = users.get_experience(user_id)
    experienceList = json.loads(experienceList.data)["results"]
    return common.response_ok(200, {
        "template": flask.render_template("experienceresults.html", experienceList=experienceList)
    })

@routes.route("/education/currentUserEducation", methods=["GET"])
def getEducation():
    user_id, err = common.authenticate(flask.request.authorization)
    if not user_id:
        return common.response_error(403, err)
    educationList = users.get_education(user_id)
    return common.response_ok(200, {
        "template": flask.render_template("educationresults.html", educationList=educationList)
    })

