import re
import sqlite3
import flask
import common
from tempfile import mkstemp
import subprocess
import os
from PyPDF2 import PdfFileReader


jobs = flask.Blueprint("jobs", __name__)

@jobs.route("/jobs", methods=["POST"])
def create_job():
    auth_user_id, err = common.authenticate(flask.request.authorization)
    if not auth_user_id:
        return common.response_error(403, err)

    json, err = common.get_json(flask.request, {
        "title": str,
        "description": (str, type(None)),
        "organization": (str, type(None)),
        "salary": (int, type(None)),
        "rate": (float, int, type(None)),
        "location": (str, type(None)),
        "tags": (list, type(None))
    })
    if not json:
        return common.response_error(400, err)

    title = json["title"]
    description = json["description"]
    organization = json["organization"]
    salary = json["salary"]
    rate = json["rate"]
    location = json["location"]
    tags = json["tags"]
    job_id = common.generate_uuid()

    if tags and not all(map(lambda x: isinstance(x, str), tags)):
        return common.response_error(400, "Invalid tags")

    db = common.get_db()
    db.execute("""
        INSERT INTO Job(id, user_id, title, description, organization, salary, rate, location)
        VALUES(?, ?, ?, ?, ?, ?, ?, ?)
    """, (job_id, auth_user_id, title, description, organization, salary, rate, location))
    if tags:
        db.executemany("""
            INSERT INTO Tag(owner_id, type, name)
            VALUES(?, 'skill', ?)
        """, map(lambda t: (job_id, t), tags))
    db.commit()

    return common.response_ok(201, {
        "job_id": job_id
    })

@jobs.route("/jobs/<job_id>", methods=["GET"])
def get_job(job_id):
    db = common.get_db()
    result = db.execute("""
        SELECT
            Job.id,
            Job.title,
            Job.description,
            Job.organization,
            Job.salary,
            Job.rate,
            Job.location
        FROM Job
        WHERE Job.id = ?
    """, (job_id,)).fetchone()
    if not result:
        return common.response_error(404, "Job not found")

    skills = list(map(lambda r: r[0], db.execute("""
        SELECT name
        FROM Tag
        WHERE owner_id = ?
        AND type = 'skill'
    """, (job_id,))))
    
    return common.response_ok(200, {
        "id": result[0],
        "title": result[1],
        "description": result[2],
        "organization": result[3],
        "salary": result[4],
        "rate": result[5],
        "location": result[6],
        "skills": skills
    })

@jobs.route("/jobs/delete", methods=["DELETE"])
def delete_job():
    auth_user_id, err = common.authenticate(flask.request.authorization)
    if not auth_user_id:
        return common.response_error(403, err)

    json, err = common.get_json(flask.request, {
        "job_id": str
    })
    if not json:
        return common.response_error(400, err)

    job_id = json["job_id"]
    query = """
        SELECT
            Job.user_id
        FROM Job
        WHERE Job.id = ?
    """
    db = common.get_db()
    result = db.execute(query, (job_id,)).fetchone()
    if not result[0] == auth_user_id:
        return common.response_error(403, "Cannot delete other user's job")

    query = """
        DELETE
        FROM Job
        WHERE Job.id = ?
    """
    db.execute(query, (job_id,))
    return common.response_ok(204, {
        "deleted_job_id": job_id
    })

@jobs.route("/jobs/search", methods=["POST"])
def query_jobs():
    json, err = common.get_json(flask.request, {
        "query": list
    })
    if not json:
        return common.response_error(400, err)
    results = query_jobs_helper(json)
    return common.response_ok(200, {
        "results": results
    })

def fragment_to_sql(frag):
    subject = frag["subject"]
    verb = frag["verb"]
    obj = frag["object"]
    if verb == "contains":
        if not isinstance(obj, str):
            raise ValueError("'contains' can only be applied to strings")
        fmt = " AND (' ' || Job.%s || ' ') LIKE ? COLLATE NOCASE" % subject
        pat = "%%%s%%"
    elif verb == "=":
        if not isinstance(obj, str):
            raise ValueError("= can only be applied to strings")
        fmt = " AND Job.%s = ?" % subject
        pat = "%s"
    elif verb == ">":
        if not isinstance(obj, (int, float)):
            raise ValueError("> can only be applied to numbers")
        fmt = " AND Job.%s >= ?" % subject
        pat = "%s"
    elif verb == "<":
        if not isinstance(obj, (int, float)):
            raise ValueError("> can only be applied to numbers")
        fmt = " AND Job.%s <= ?" % subject
        pat = "%s"
    else:
        raise ValueError("Invalid verb")
    return (fmt, pat, obj)

def query_jobs_helper(json):
    # Convert query to SQL
    query = json["query"]
    sql_frags = []
    tags = []
    for fragment in query:
        fmt = fragment
        if fmt["subject"] == "tags":
            if fmt["verb"] != "contains":
                return common.response_error(400, "Invalid query")
            obj = fmt["object"]
            if not isinstance(obj, str):
                return common.response_error(400, "Tag must be string")
            tags.append(obj)
        else:
            try:
                sql_frags.append(fragment_to_sql(fragment))
            except ValueError as e:
                return common.response_error(400, str(e))

    # Base query
    query = """
        SELECT
            Job.id,
            Job.title,
            Job.description,
            Job.organization,
            Job.salary,
            Job.rate,
            Job.location
        FROM Job
    """
    params = []

    # If user filtered on tags, we need to join
    # on the tag table too
    if tags:
        query += """
            JOIN Tag
            ON Job.id = Tag.owner_id
            WHERE Tag.name in (%s)
        """ % (", ".join(["?"] * len(tags)),)
        params += tags
    else:
        query += "WHERE 1=1 "

    # Merge query fragments
    for q, p, v in sql_frags:
        query += q
        params.append(p % v)

    # If filtering on tags, also need a group-by.
    # We want to match if ALL tags are present, so
    # we just group by the job, then filter out the
    # ones where the number of tags is not the same
    # as the ones queried.
    if tags:
        query += """
            GROUP BY Tag.owner_id
            HAVING COUNT(Tag.name) = ?
        """
        params.append(len(tags))

    # Then just execute the query, ez pz
    results = []
    db = common.get_db()
    for row in db.execute(query, tuple(params)):
        skills = list(map(lambda r: r[0], db.execute("""
            SELECT name
            FROM Tag
            WHERE owner_id = ?
            AND type = 'skill'
        """, (row[0],))))
        results.append({
            "id": row[0],
            "title": row[1],
            "description": row[2],
            "organization": row[3],
            "salary": row[4],
            "rate": row[5],
            "location": row[6],
            "skills": skills
        })
    
    return results

# Retrieve all jobs by a given user
def get_user_jobs(user_id):
    query = """
        SELECT
            Job.id,
            Job.title,
            Job.location,
            Job.salary,
            Job.rate,
            Job.description
        FROM Job
        WHERE Job.user_id = ?
    """
    results = []
    db = common.get_db()
    query_results = db.execute(query, (user_id,))
    for row in query_results:
        results.append({
            "id": row[0],
            "title": row[1],
            "location": row[2],
            "salary": row[3],
            "rate": row[4],
            "description": row[5]
        })
        skills = list(map(lambda r: r[0], db.execute("""
            SELECT Tag.name
            FROM Tag
            WHERE Tag.owner_id = ?
        """, (results[0]["id"],))))
        results[-1]["tags"] = skills
    return results

# Return a PDF as an attachment that's optimized
# for the POSTing user to apply for the specified
# job.
#
# Current plan is to dump profile info as markdown
# or RST, then use an external tool to generate the
# PDF file. Should intersect user's skills/experience
# with the skill tags of the required jobs, then fall
# back to all skills/experience if there are too few
# matches to fill up a page.
@jobs.route("/jobs/<job_id>/generate_resume/<user_id>", methods=["GET"])
def generate_resume(job_id, user_id):
    auth_user_id = user_id
    # auth_user_id, err = common.authenticate(flask.request.authorization)
    # if not auth_user_id:
    #     return common.response_error(403, err)
    db = common.get_db()
    job = db.execute("""
        SELECT
            Job.id,
            Job.title,
            Job.description,
            Job.organization,
            Job.salary,
            Job.rate,
            Job.location
        FROM Job
        WHERE Job.id = ?
    """, (job_id,)).fetchone()
    if not job:
        return common.response_error(404, "Job not found")

    job_skills = list(map(lambda r: r[0], db.execute("""
        SELECT name
        FROM Tag
        WHERE owner_id = ?
        AND type = 'skill'
    """, (job_id,))))
    
    profile = db.execute("""
        SELECT email, name, phone, website, biography
        FROM User
        WHERE id = ?
        LIMIT 1
    """, (auth_user_id,)).fetchone()
    user_skills = []
    user_unrelevant_skills = []
    user_skills_query = db.execute("""
        SELECT name
        FROM Tag
        WHERE owner_id = ?
        AND type = 'skill'
    """, (auth_user_id,))
    ##get all relevant skills
    for row in user_skills_query:
        if row[0] in job_skills and row[0] not in user_skills:
            user_skills.append(row[0])
        if row[0] not in job_skills and row[0] not in user_unrelevant_skills:
            user_unrelevant_skills.append(row[0])
    experiences_query_string = """
        SELECT id, title, description, organization, location
        FROM UserExperience
        """
    if len(job_skills) > 0:
        experiences_query_string += """
        JOIN Tag
        ON id = Tag.owner_id
        WHERE user_id = ?
        AND Tag.name in (%s)
        GROUP BY Tag.Owner_id
        ORDER BY Count(Tag.Owner_id) DESC
    """ % (", ".join(["?"] * len(job_skills)),)
    else:
        experiences_query_string += """
        WHERE user_id = ?
    """
    experiences_query = db.execute(experiences_query_string, ((auth_user_id,) + tuple(job_skills)))
    ##add all experience skills to list of skills
    experiences = []
    for row in experiences_query:
        experience_skills = list(map(lambda r: r[0], db.execute("""
            SELECT name
            FROM Tag
            WHERE owner_id = ?
            AND type = 'skill'
        """, (row[0],))))
        for skill in experience_skills:
            if skill in job_skills and skill not in user_skills:
                user_skills.append(skill)
            if skill not in job_skills and skill not in user_unrelevant_skills:
                user_unrelevant_skills.append(skill)
        experiences.append(row)
    education = db.execute("""
        SELECT id, school_name, level, gpa, major
        FROM UserEducation
        WHERE user_id = ?
    """, (auth_user_id,))
    user_skills.extend(user_unrelevant_skills)
    edu2 = []
    for row in education:
        edu2.append(row)

    num_experiences = min(len(experiences), 1)
    num_skills = min(len(user_skills), 1)

    markdown = """{}
============

---------
{}
{}
{}
---------

Biography
---------

{}


Education
---------
""".format(profile[1], profile[0], profile[3], profile[2], profile[4])
    for row in edu2:
        markdown += "**{}, {}**; {} (GPA: {})\n".format(row[2], row[4], row[1], row[3])
    markdown += """
Experience
----------
"""
    for i in range(num_experiences):
        row = experiences[i]
        markdown += """**{} at {} in {}**

* {}
""".format(row[1], row[3], row[4], row[2])
    markdown += """
Skills
----------------------------------------
"""
    for i in range(num_skills):
        markdown += "*{}\n".format(user_skills[i])
    fd, fname = mkstemp(suffix='.pdf')
    subprocess.run(["pandoc", "-o", fname, "--from", "markdown-yaml_metadata_block", "-V",  "geometry:margin=1in"], input=markdown, encoding='utf-8')
    while True:
        if num_experiences == len(experiences) and num_skills == len(user_skills):
            break
        if num_experiences < len(experiences):
            num_experiences += 1
        if num_skills < len(user_skills):
            num_skills += 1
        oldmarkdown = markdown
        markdown = """{}
============

---------
{}
{}
{}
---------

Biography
---------

{}


Education
---------
""".format(profile[1], profile[0], profile[3], profile[2], profile[4])
        for row in edu2:
            markdown += "**{}, {}**; {} (GPA: {})\n".format(row[2], row[4], row[1], row[3])
        markdown += """
Experience
----------
"""
        for i in range(num_experiences):
            row = experiences[i]
            markdown += """**{} at {} in {}**

* {} \n
""".format(row[1], row[3], row[4], row[2])
        markdown += """
Skills
----------------------------------------
"""
        for i in range(num_skills):
            markdown += "* {}\n".format(user_skills[i])
        subprocess.run(["pandoc", "-o", fname, "--from", "markdown-yaml_metadata_block", "-V",  "geometry:margin=1in"], input=markdown, encoding='utf-8')
        pages = 0
        with open(fname, 'rb') as pdf:
            pages = PdfFileReader(pdf).getNumPages()
        if pages > 1:
            subprocess.run(["pandoc", "-o", fname, "--from", "markdown-yaml_metadata_block", "-V",  "geometry:margin=1in"], input=oldmarkdown, encoding='utf-8')
            break

    res = flask.send_file(fname, attachment_filename='resume.pdf')
    os.close(fd)
    #os.remove(fname)
    return res
