import re
import sqlite3
import flask
import common

applications = flask.Blueprint("applications", __name__)

@applications.route("/jobs/<job_id>/applications", methods=["POST"])
def submit_application(job_id):
    auth_user_id, err = common.authenticate(flask.request.authorization)
    if not auth_user_id:
        return common.response_error(403, err)

    # Cover letter is optional for those who are too lazy
    # (like me, for example)
    json, err = common.get_json(flask.request, {
        "cover_letter": (str, type(None))
    })
    if err:
        return common.response_error(400, err)
    cover_letter = json["cover_letter"]

    # Insert row into db
    db = common.get_db()
    try:
        db.execute("""
            INSERT INTO Application(user_id, job_id, cover_letter)
            VALUES(?, ?, ?)
        """, (auth_user_id, job_id, cover_letter))
        db.commit()
    except sqlite3.IntegrityError:
        return common.response_error(400, "Invalid job ID or duplicate application")

    return common.response_ok(200)

@applications.route("/jobs/<job_id>/applications", methods=["GET"])
def get_applications(job_id):
    auth_user_id, err = common.authenticate(flask.request.authorization)
    if not auth_user_id:
        return common.response_error(403, err)

    return common.response_ok(200, {
        "applications": get_applications_helper(job_id, auth_user_id)
    })

def get_applications_helper(job_id, user_id):
    db = common.get_db()

    # Note: as a side effect of this, if someone who is
    # not allowed to view the data performs this query,
    # they will see no results. It's too much effort to
    # return an appropriate error message.
    query = db.execute("""
        SELECT User.id, email, name, phone, website, biography, cover_letter
        FROM User
        JOIN Application ON User.id = Application.user_id
        JOIN Job ON Job.id = Application.job_id
        WHERE Application.job_id = ?
        AND Job.user_id = ?
    """, (job_id, user_id))

    # Convert rows to JSON array
    applications = []
    for row in query:
        skills = list(map(lambda r: r[0], db.execute("""
            SELECT name
            FROM Tag
            WHERE owner_id = ?
            AND type = 'skill'
        """, (row[0],))))

        applications.append({
            "user_id": row[0],
            "email": row[1],
            "name": row[2],
            "phone": row[3],
            "website": row[4],
            "biography": row[5],
            "cover_letter": row[6],
            "skills": skills
        })

    return applications