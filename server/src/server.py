#!/usr/env/bin python3
import sys
import flask

import common
import routes
import users
import jobs
import applications

# If you add a Flask module, register it here
app = flask.Flask(__name__)
app.register_blueprint(users.users)
app.register_blueprint(routes.routes)
app.register_blueprint(jobs.jobs)
app.register_blueprint(applications.applications)

def main():
    port = 8080
    if len(sys.argv) > 1:
        port = int(sys.argv[1])
    app.run(host="0.0.0.0", port=port)
    return 0

if __name__ == "__main__":
    sys.exit(main())
