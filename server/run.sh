#!/bin/sh

root_dir=$(readlink -e "$(dirname $0)")
data_dir="$root_dir/data"
src_dir="$root_dir/src"
mkdir -p "$data_dir"

# If first arg is reset, delete database
if [ "$1" == "reset" ]; then
    echo "Deleting database"
    rm "$data_dir/data.db"
    shift
fi

# Create database if not already exists
if [ ! -f "$data_dir/data.db" ]; then
    sqlite3 "$data_dir/data.db" < "$data_dir/schema.sql"
    sqlite3 "$data_dir/data.db" < "$data_dir/dump_test.sql"
    sqlite3 "$data_dir/data.db" < "$data_dir/dump_real.sql"
fi

# Run the server
(
    cd "$root_dir"
    export FLASK_APP="$src_dir/server.py"
    export FLASK_DEBUG=1
    python3 -m flask run --host 0.0.0.0
)
