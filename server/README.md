# JobGoblin server

Database stuff goes into `data/`. Code goes into `src/`.

## Dependencies

- Python 3
- Flask
- SQLite
- PyPDF
- Pandoc

## Running

```
# Reset the database
$ ./run.sh reset

# Start the server
$ ./run.sh
```

## API

```
# Set up args (adjust as necessary)
server_url=http://127.0.0.1:5000

# Create a user
curl -X POST $server_url/users \
    -d '{"email": "test@example.com", "password": "My_pAsSw0rD", "name": "Foo Bar"}'

# Log in
curl -X POST $server_url/login \
    -u foo@bar.com:p@55w0rd

# Retrieve user profile
curl -X GET $server_url/users/test_user_id

# Update user profile
curl -X PUT $server_url/users/test_user_id \
    -u foo@bar.com:p@55w0rd \
    -d '{"skills": ["herblore", "runecrafting"]}'

# Create a job listing
curl -X POST $server_url/jobs \
    -u foo@bar.com:p@55w0rd \
    -d '{"title": "Software Engineer", "tags": ["code"]}'

# Get details of job listing
curl -X GET $server_url/jobs/test_job_id

# Create a job application
curl -X POST $server_url/jobs/test_job_id/applications \
    -u foo@bar.com:p@55w0rd \
    -d '{"cover_letter": "I am applying here because my mom told me to"}'

# Get job applications
curl -X GET $server_url/jobs/test_job_id/applications \
    -u foo@bar.com:p@55w0rd
```
