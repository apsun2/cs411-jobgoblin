import pandas as pd
technologies = ["java", "c++", "c", "c#", "python", "javascript", "verilog", \
"firmware", "eletrical", "teacher", "teach", "education", "math", "mechnical", \
"children", "leadership", "front-end", "back-end","crawler", \
"ios", "android", "automotive"]
df = pd.read_csv("out_final.csv")
descs = df['description']
tags = []
for row in descs:
	current_row_tag = []
	for t in technologies:
		if t in row.replace('/', ' ').replace(',', ' ').replace('.', ' ').lower().split(' '):
			current_row_tag.append(t)
	tags.append(', '.join(current_row_tag))
tags_pd = pd.Series(tags)
df['tags'] = tags_pd.values
df['id'] = df.index
df.to_csv('out_final_tags.csv', encoding='utf-8', index=False)