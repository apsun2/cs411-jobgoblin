# -*- coding:utf-8 -*-


import requests
from bs4 import BeautifulSoup
import lxml
from selenium import webdriver
#from selenium.webdriver.firefox.options import Options
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import numpy as np
from time import sleep
import pandas as pd
import re
import csv
import os

unpaid = ["paid", "Paid", "Unpaid", "unpaid", "pay", "Pay"]


def parse_detail_page(webpage, df, index):
    print("begin parsing")

    soup = BeautifulSoup(webpage, "lxml")
    pending_parsing = soup.prettify()

    title = ""
    company = ""
    location = ""
    description = ""
    compensation = ""
    desc_flag = False
    level = 0
    openLineReg = re.compile(r'<[a-z]')
    closeLineReg = re.compile(r'</[a-z]')
    page_list = iter(pending_parsing.splitlines())
    for line in page_list:
        #print(line)
        if "style__heading___2pE2l style__large___1xS9U" in line and title == "":
            title = next(page_list).strip()
        if "style__heading___2pE2l style__medium___Aq71d" in line and company == "":
            while openLineReg.search(line) or closeLineReg.search(line):
                line = next(page_list)
            company = line.strip()
        if "style__list-with-tooltip___2c5rW" in line and location == "":
            while openLineReg.search(line) or closeLineReg.search(line):
                line = next(page_list)
            location = line.strip()
        if "pay-rate" in line and compensation == "":
            while openLineReg.search(line) or closeLineReg.search(line):
                line = next(page_list)
            raw_compensation = line.strip().replace('$', '').replace(',', '')
            if any(p in raw_compensation for p in unpaid):
                return
            try:
                if "hour" in raw_compensation:
                    compensation = [float(s) for s in raw_compensation.split(' ') if parse_float(s)][0]*40*4*12
                elif "day" in raw_compensation:
                    compensation = [float(s) for s in raw_compensation.split(' ') if parse_float(s)][0]*5*4*12
                elif "week" in raw_compensation:
                    compensation = [float(s) for s in raw_compensation.split(' ') if parse_float(s)][0]*4*12
                elif "month" in raw_compensation:
                    compensation = [float(s) for s in raw_compensation.split(' ') if parse_float(s)][0]*12
                elif "half" in raw_compensation:
                    compensation = [float(s) for s in raw_compensation.split(' ') if parse_float(s)][0]*2
                else:
                    compensation = [float(s) for s in raw_compensation.split(' ') if parse_float(s)][0]
            except Exception as e:
                print(raw_compensation)
                compensation = raw_compensation

        if "style__job-description___17MNY" in line:
            desc_flag = True
        if desc_flag:
            if openLineReg.search(line):
                level+=1
                continue
            elif closeLineReg.search(line):
                level-=1
            else:
                if "<!--" not in line: #get rid of react lines
                    description += line.strip()
                continue
            if level == 0:
                break

    d = {'JobTitle':title, 'Company':company, 'description':description, 
        'Location':location, 'Compensation':compensation}

    print("parsing done")
    return d

def parse_float(str):
    try:
        float(str)
        return True
    except Exception as e:
        return False

def xiaopachong():
    

    chromedriver = r"D:\chromedriver.exe"
    # os.environ["webdriver.chrome.driver"] = chromedriver
    df = pd.DataFrame(columns=['JobTitle', 'Company', 'description', 
        'Location', 'Compensation'])
    message_list = []
    #use below for using ff
    #options = Options()
    #options.add_argument("--headless")
    #driver = webdriver.Firefox(firefox_options=options)

    #use below for using headless chrome in linux
    # chrome_options = Options()
    # chrome_options.add_argument('headless')
    # chrome_options.add_argument(' --no-sandbox')
    # driver = webdriver.Chrome(chromedriver, chrome_options=chrome_options)

    driver = webdriver.Chrome(chromedriver)

    # Service selection
    # 364 is the shibboleth code for handshake
    driver.get("https://app.joinhandshake.com/auth?auth=364")

    

    # Fill the login form and submit it
    driver.find_element_by_id('j_username').send_keys("YOURNETID")
    driver.find_element_by_id('j_password').send_keys("YOURPASSWORD")
    driver.find_element_by_id('submit_button').click()

    driver.save_screenshot('login.png')
    #wait for shibboleth to jump
    sleep(3)
    print("logged in")

    sleep(3)
    #driver.save_screenshot('mainpage.png')
    driver.get("https://illinois.joinhandshake.com/postings")
    sleep(3)
    try:
        WebDriverWait(driver, 6).until(EC.alert_is_present(),
                                   'Timed out waiting for PA creation ' +
                                   'confirmation popup to appear.')
        alert = driver.switch_to_alert()
        print(alert.text)
        alert.accept()
    except TimeoutException as e:
        pass
    driver.save_screenshot('listing.png')
    print("job page opened")

    #print (driver.page_source.encode("utf-8"))
    #now save the main window
    main_window = driver.current_window_handle

    #save base url
    base_url = driver.current_url

    for page in range(2, 47):
        #css selector
        list_links = [link.get_attribute('href') for link in driver.find_elements_by_css_selector("a[href*='jobs']")]
        for i in range(len(list_links)):
            #opens a new window and goes to that window
            try:
                #use below for ff
                # print("entering"+str(i))
                # sleep(2)
                # links[i].send_keys(Keys.CONTROL + Keys.RETURN)
                # print("clicked")
                # sleep(5)
                # driver.switch_to_window(driver.window_handles[1])
                # print("switched")
                # sleep(3)
                # driver.save_screenshot('screen'+str(i)+'.png')
                # sleep(2)
                # print("parsing"+str(i))
                # df1 = parse_detail_page(driver.page_source, df, i)
                # driver.save_screenshot('screen'+str(i)+'.png')
                # driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 'w')
                # sleep(3)
                # driver.switch_to_window(driver.window_handles[0])
                # print(driver.page_source)
                # break;
                # print("done"+str(i))
                # print(df1)
                # df1.to_csv('test'+str(i)+'.csv')
                # df.append(df1)
                # sleep(np.random.uniform(15,20))

                # new_links = driver.find_elements_by_css_selector("a[href*='jobs']")
                # if len(new_links) <= i:
                #     break
                # new_links[i].click()

                driver.get(list_links[i])
                print("clicked")
                sleep(np.random.uniform(1,3))
                print("parsing"+str(i))
                d = parse_detail_page(driver.page_source, df, i)
                print("done"+str(i))
                driver.back()
                #try this if back() not functional
                #driver.execute_script("window.history.go(-1)")
                #sleep(5)
                

                if d is not None:
                    message_list.append(d)
                    print(d)
                    
                sleep(np.random.uniform(2,6))

            except Exception as e:
                #print(driver.page_source)
                driver.save_screenshot('screen.png')
                print(e)
                print("error")
                break
                # driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 'w')
                # sleep(1)
                # driver.switch_to_window(driver.window_handles[0])
            
            
        try:
            print("going to page {0}".format(page))
            driver.get(base_url + "?page="+str(page)+"&per_page=25&sort_direction=desc&sort_column=default")
            sleep(np.random.uniform(1,3))
            if(len(message_list) != 0):
                keys = message_list[0].keys()
                with open('out.csv', 'w', encoding='utf-8') as output_file:
                    dict_writer = csv.DictWriter(output_file, keys)
                    dict_writer.writeheader()
                    dict_writer.writerows(message_list)
        except Exception as e:
            driver.save_screenshot('screen.png')
            print(e)
            print("error")
            break

    driver.quit()

    if(len(message_list) != 0):
        keys = message_list[0].keys()
        with open('out.csv', 'w', encoding='utf-8') as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(message_list)
    
    print("done")            

    
if __name__ == '__main__':
    xiaopachong();
        
