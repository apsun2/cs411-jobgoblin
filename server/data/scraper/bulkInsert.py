import pandas as pd
import numpy as np
import sqlite3
connex = sqlite3.connect("data.db")
df = pd.read_csv("out_final_tags.csv")
df = df.loc[:, ~df.columns.str.contains('^Unnamed')]

df['user_id'] = 'test_user_id'
df.rename(columns={'JobTitle': 'title', 'Company': 'organization'\
	, 'Compensation': 'salary', 'Location': 'location'}, inplace=True)
df_tags = df[['id', 'tags']]
df.drop(columns=['tags'], inplace=True)
df_tags.rename(columns={'id': 'owner_id', 'tags': 'name'}, inplace=True)
df_tags['type'] = 'skill'
df_tags['name'].replace('', np.nan, inplace=True)
df_tags.dropna(subset=['name'], inplace=True)
df.to_sql(name='Job', con=connex, if_exists='append', index=False)
df_tags.to_sql(name='Tag', con=connex, if_exists='append', index=False)
connex.commit()
connex.close()