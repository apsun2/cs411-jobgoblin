import pandas as pd
import numpy as np
import sqlite3
import random
import uuid
import binascii
import hashlib

def generate_uuid():
    return uuid.uuid4().hex

def phn():
    n = '0000000000'
    while '9' in n[3:6] or n[3:6]=='000' or n[6]==n[7]==n[8]==n[9]:
        n = str(random.randint(10**9, 10**10-1))
    return n[:3] + '-' + n[3:6] + '-' + n[6:]

def grade():
	return random.randint(20, 40)/10

def hash_password(pwd, salt):
    return binascii.hexlify(
        hashlib.pbkdf2_hmac(
            "sha256",
            pwd.encode("utf-8"),
            binascii.unhexlify(salt),
            100000))


connex = sqlite3.connect("data.db")
df = pd.read_csv("user.csv")
df = df.loc[:, ~df.columns.str.contains('^Unnamed')]

df.rename(columns={'Name': 'name', 'CurrentSchool': 'school_name'\
	, 'HighestDeg': 'level' }, inplace=True)
password = '123456'
phone_list = []
salt_list = []
user_id_list = []
exp_id_list = []
education_id_list = []
gpa_list = []
major_list = []
pwd_list = []
email_list = []
name_list = df['name']
for i in range(10):
	phone_list.append(phn())
	salt_list.append(generate_uuid())
	user_id_list.append(generate_uuid())
	education_id_list.append(generate_uuid())
	gpa_list.append(grade())
	major_list.append("ECE")
	exp_id_list.append(generate_uuid())
	pwd_list.append(hash_password(password, salt_list[i]))
	email_list.append(name_list[i].replace(' ', '').lower()+'@illinois.edu')

tag_list = []
for row in df['Skills']:
	tag_list.append(row.replace(';', ', '))
df['Skills'] = pd.Series(tag_list).values

title_list = []
org_list = []
desc_list = []
for row in df['Experience']:
	for exp in row.split(';'):
		info = exp.split('@')
		if(len(info) < 2):
			title_list.append('none')
			org_list.append('none')
			continue
		title_list.append(info[0])
		org_list.append(info[1])
		break;
User_df = df[['name']]
User_df['id'] = pd.Series(user_id_list).values
User_df['password_salt'] = pd.Series(salt_list).values
User_df['password_hash'] = pd.Series(pwd_list).values
User_df['phone'] = pd.Series(phone_list).values
User_df['email'] = pd.Series(email_list).values
User_df.to_sql(name='User', con=connex, if_exists='append', index=False)

User_edu_df = df[['school_name', 'level']]
User_edu_df['user_id'] = pd.Series(user_id_list).values
User_edu_df['id'] = pd.Series(education_id_list).values
User_edu_df['gpa'] = pd.Series(gpa_list).values
User_edu_df['major'] = pd.Series(major_list).values
User_edu_df.to_sql(name='UserEducation', con=connex, if_exists='append', index=False)

User_exp_df = df[['Experience']]
User_exp_df.rename(columns={'Experience': 'description'}, inplace=True)
User_exp_df['user_id'] = pd.Series(user_id_list).values
User_exp_df['id'] = pd.Series(exp_id_list).values
User_exp_df['title'] = pd.Series(title_list).values
User_exp_df['organization'] = pd.Series(org_list).values
User_exp_df.to_sql(name='UserExperience', con=connex, if_exists='append', index=False)

tags_df = df[['Skills']]
tags_df['owner_id'] = pd.Series(user_id_list).values
tags_df['type'] = 'skill'
tags_df.rename(columns={'Skills': 'name'}, inplace=True)
tags_df.to_sql(name='Tag', con=connex, if_exists='append', index=False)

connex.commit()
connex.close()
