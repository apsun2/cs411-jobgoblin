-- User information
CREATE TABLE User(
    id TEXT PRIMARY KEY NOT NULL,
    email TEXT UNIQUE NOT NULL,
    password_salt TEXT NOT NULL,
    password_hash TEXT NOT NULL,
    name TEXT NOT NULL,
    phone TEXT,
    website TEXT,
    biography TEXT
);

-- Login tokens for authentication
CREATE TABLE LoginToken(
    user_id TEXT NOT NULL,
    token TEXT NOT NULL,
    FOREIGN KEY(user_id) REFERENCES User(id)
);

-- User's education information
-- Since a user may have gone to multiple schools,
-- we have a Education -> User relationship.
CREATE TABLE UserEducation(
    id TEXT PRIMARY KEY NOT NULL,
    user_id TEXT NOT NULL,
    school_name TEXT NOT NULL,
    level TEXT,
    gpa REAL,
    major TEXT,
    FOREIGN KEY(user_id) REFERENCES User(id)
);

-- User's experience (both work and project) information
-- Experience -> User
CREATE TABLE UserExperience(
    id TEXT PRIMARY KEY NOT NULL,
    user_id TEXT NOT NULL,
    title TEXT NOT NULL,
    description TEXT,
    organization TEXT,
    location TEXT,
    FOREIGN KEY(user_id) REFERENCES User(id)
);

-- Job listing information
CREATE TABLE Job(
    id TEXT PRIMARY KEY NOT NULL,
    user_id TEXT NOT NULL,
    title TEXT NOT NULL,
    description TEXT,
    organization TEXT,
    salary INTEGER,
    rate REAL,
    location TEXT,
    FOREIGN KEY(user_id) REFERENCES User(id)
);

-- Job application information
CREATE TABLE Application(
    user_id TEXT NOT NULL,
    job_id TEXT NOT NULL,
    cover_letter TEXT,
    FOREIGN KEY(user_id) REFERENCES User(id),
    FOREIGN KEY(job_id) REFERENCES Job(id) ON DELETE CASCADE,
    PRIMARY KEY(user_id, job_id)
);

-- Generic "tag", used in all locations
-- Mostly used as a "skill" tag. Since multiple
-- things use tags (user profile, job listing,
-- experience info), the owner_id may be many
-- types, so we don't use a foreign key.
CREATE TABLE Tag(
    owner_id TEXT NOT NULL,
    type TEXT NOT NULL,
    name TEXT NOT NULL
);
