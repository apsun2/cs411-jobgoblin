PRAGMA foreign_keys=ON;
BEGIN TRANSACTION;
INSERT INTO User VALUES(
    'test_user_id',
    'foo@bar.com',
    '3bcff597347e4e3ab2e539a088bdc9ff',
    X'64383265646138613934623835356437373034633133336636623039626365353431383437383436633934626366343361396161393639643432373237643735',
    'John Smith',
    '123-555-5555',
    'bar.com',
    'I build stuff for fun'
);
INSERT INTO Job VALUES(
    'test_job_id',
    'test_user_id',
    'Software Engineer',
    'Build stuff, get money',
    'Goggle',
    100000,
    42.0,
    'Earth'
);
INSERT INTO Tag VALUES(
    'test_job_id',
    'skill',
    'sleeping'
);
INSERT INTO Tag VALUES(
    'test_job_id',
    'skill',
    'eating'
);
INSERT INTO Tag VALUES(
    'test_user_id',
    'skill',
    'fishing'
);
INSERT INTO Tag VALUES(
    'test_user_id',
    'skill',
    'runecrafting'
);
COMMIT;
